import React from 'react'
import './styles/App.css'
import Tree from './component/Tree/Tree'



function App() {
  return (
    <div className="App" style={{display: "flex", justifyContent: "center"}}>
      <Tree />      
    </div>
  )
}

export default App
