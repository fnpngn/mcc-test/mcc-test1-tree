import React, { useEffect, useMemo, useState } from 'react'
import TreeNodeData from '../data/TreeNodeData'
import TreeNodeWrapper from '../data/TreeNodeWrapper'
import Tree from '../data/Tree'

interface TreeContextActions {
  setNodes: React.Dispatch<React.SetStateAction<TreeNodeWrapper>>
  setOpenNodeIds:  React.Dispatch<React.SetStateAction<number[]>>
  setEditingNodeIds:  React.Dispatch<React.SetStateAction<number[]>>
  setSelectedNodeId:  React.Dispatch<React.SetStateAction<number | null>>
}
const TreeContext = React.createContext<Tree & TreeContextActions | undefined>(undefined)

function TreeContextProvider({
  children, 
  _nodes, 
}: React.PropsWithChildren<{
  _nodes: TreeNodeData[]
}>) {
  //const [tree, setTree] = useState(Tree.fromNodes(new TreeNodeWrapper(_nodes)))
  const [nodes, setNodes] = useState(new TreeNodeWrapper(_nodes))
  const [openNodeIds, setOpenNodeIds] = useState<number[]>([])
  const [editingNodeIds, setEditingNodeIds] = useState<number[]>([])
  const [selectedNodeId, setSelectedNodeId] = useState<number | null>(0)
  
  useEffect(() => {
    setNodes(new TreeNodeWrapper(_nodes))
  }, [_nodes])

  return (
    <TreeContext.Provider value={{
        nodes, 
        openNodeIds, 
        editingNodeIds, 
        selectedNodeId, 
        setNodes, 
        setOpenNodeIds, 
        setEditingNodeIds, 
        setSelectedNodeId 
      }}>
      {children}
    </TreeContext.Provider>
  )
}
  
  export default TreeContext
  export { TreeContextProvider }