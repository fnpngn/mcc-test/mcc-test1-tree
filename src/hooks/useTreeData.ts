import React, { useContext } from 'react'
import TreeContext from '../context/TreeContext'

function useTreeData() {
  const context = useContext(TreeContext)
  if(context === undefined){
    throw new Error("Tree context does not have a provider")
  }
  return context
}

export default useTreeData