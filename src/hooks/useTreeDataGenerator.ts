import React from 'react'
import TreeNodeData from '../data/TreeNodeData'

function useTreeDataGenerator() {
    const populateChildren = (nodes: TreeNodeData[], 
        parent: TreeNodeData, 
        genAmount: () => number, 
        indexWrapper: {index: number}, 
        depthLeft: number
      ) => {
      let children: TreeNodeData[] = []
      for(let i = 0, node: TreeNodeData; i< genAmount(); i++){
        node = new TreeNodeData()
        node.parentId = parent.id
        node.label = `Node ${indexWrapper.index++}`
        children.push(node)
      }
      parent.childrenIds = children.map(x => x.id)
      nodes.push(...children)
      if(depthLeft === 0){
        return
      }
      children.forEach(child => populateChildren(nodes, child, genAmount, indexWrapper, depthLeft-1))
    }

    const generate = (depth: number, avgWidth: number) => {
      let data: TreeNodeData[] = [{id: 0, childrenIds: [], content: null, label: "root", parentId: 0}]
      let parent = data[0];
      let index: number = 0
      
      populateChildren(data, parent, () => Math.round((Math.random() + 0.5) * avgWidth), {index: index}, depth)
      return data
    }

  return generate
}

export default useTreeDataGenerator