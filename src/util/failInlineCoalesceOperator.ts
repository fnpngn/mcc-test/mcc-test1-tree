function fail<T>(message: string = ""): T{
    throw new Error(message)
}

export default fail