class OperationResult<T> {
    content: T | null
    success: boolean
    message: string = ""

    constructor(content: T | null, success: boolean = true, message: string = ""){
        this.content = content
        this.success = success
        this.message = message
    }

    static success<T>(content: T){
        return new this(content)
    }
    static fail(message: string){
        return new this(null, false, message)
    }
}

export default OperationResult