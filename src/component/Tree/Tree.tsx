import React, { useMemo, useState } from 'react'
import '../../styles/Tree.css'
import TreeRoot from './TreeRoot'
import TreeWrapper from './TreeWrapper'
import { TreeContextProvider } from '../../context/TreeContext'
import useTreeDataGenerator from '../../hooks/useTreeDataGenerator'

// const testTreeData: TreeNodeData[] = [
//     {
//       id: 0,
//       parentId: 0,
//       childrenIds: [1],
//       content: null,
//       label: "label0"
//     },
//     {
//       id: 1,
//       parentId: 0,
//       childrenIds: [2,3],
//       content: (<div>content of 1</div>),
//       label: "label1"
//     },
//     {
//       id: 2,
//       parentId: 1,
//       childrenIds: [4],
//       content: null,
//       label: "label2"
//     },
//     {
//       id: 3,
//       parentId: 1,
//       childrenIds: [],
//       content: null,
//       label: "label3"
//     },
//     {
//       id: 4,
//       parentId: 2,
//       childrenIds: [],
//       content: (<>lol</>),
//       label: "label3"
//     }
//   ]

function Tree() {
  const generate = useTreeDataGenerator()
  const data = useMemo(() => generate(2, 4), [generate])

  return (
    <TreeContextProvider _nodes={data}>
      <TreeWrapper defaultData={data}>
          <TreeRoot />
      </TreeWrapper>
    </TreeContextProvider>
  )
}

export default Tree
// export { testTreeData }