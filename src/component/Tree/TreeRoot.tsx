import React, { useMemo } from 'react'
import TreeNodeData from '../../data/TreeNodeData'
import TreeNode from './TreeNode'
import useTreeData from '../../hooks/useTreeData'
import fail from '../../util/failInlineCoalesceOperator'


function TreeRoot() {
  const { nodes } = useTreeData()
  const root = useMemo(() => nodes.getRootNode(), [nodes])
  return (
    <div id='tree-node-list-top-container'>
      {nodes.getLength() <= 0 && 
        <div>Tree Empty</div>
      }
      <ul className='tree-node-list'>
          {
            root.childrenIds.map(id => (
              <TreeNode id={id} key={id} />
            ))
          }
      </ul>
    </div>
  )
}

export default TreeRoot
