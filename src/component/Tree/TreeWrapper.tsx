import React, { useState } from 'react'
import TreeRoot from './TreeRoot'
import useTreeData from '../../hooks/useTreeData'
// import { testTreeData } from './Tree'
import TreeNodeData from '../../data/TreeNodeData'
import useTreeDataGenerator from '../../hooks/useTreeDataGenerator'
import CustomCheckbox from '../CustomCheckbox'
import TreeNodeWrapper from '../../data/TreeNodeWrapper'

function TreeWrapper({ children, defaultData } : {children: JSX.Element, defaultData: TreeNodeData[]}) {
  const { 
    nodes, 
    selectedNodeId, 
    editingNodeIds,
    setNodes,
    setOpenNodeIds,
    setEditingNodeIds,
    setSelectedNodeId
  } = useTreeData()

  const [cascadeChildren, setCascadeChildren] = useState(false)
  
  const handleAdd = () => {
    let parentId = 0
    if(selectedNodeId === null){
      alert("node not selected")
    } else {
      parentId = selectedNodeId
    }
    let node = new TreeNodeData()
    node.parentId = parentId
    node.childrenIds = []
    setNodes(prev => prev.addNode(node))
  }

  const handleRemove = () => {
    if(selectedNodeId === null || selectedNodeId === 0){
      alert("select a node first")
      return
    }
    setNodes(prev => prev.deleteNode(selectedNodeId, cascadeChildren))
    setSelectedNodeId(0)
  }

  const handleEdit = () => {
    if(selectedNodeId !== null && editingNodeIds.includes(selectedNodeId) === false){
      setEditingNodeIds(prev => prev.concat(selectedNodeId))
    }
  }

  const handleReset = () => {
    setEditingNodeIds([])
    setOpenNodeIds([])
    setSelectedNodeId(0)
    setNodes(new TreeNodeWrapper(defaultData))
  }

  return (
    <div className='tree'>
        {children}
        <div className='tree-controls'>
          <button className='tree-controls-btn' onClick={handleAdd}>Add</button>
          <button className='tree-controls-btn' onClick={handleRemove}>Remove</button>
          <button className='tree-controls-btn' onClick={handleEdit}>Edit</button>
          <button className='tree-controls-btn' onClick={handleReset}>Reset</button>
        </div>
        <div className='tree-controls-additional'>
          <CustomCheckbox 
            defaultValue={cascadeChildren} 
            label='Cascade Delete Children' 
            onChange={val => setCascadeChildren(val)}
          />
        </div>
    </div>
  )
}

export default TreeWrapper