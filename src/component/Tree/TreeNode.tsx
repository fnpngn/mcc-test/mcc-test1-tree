import React, { useEffect, useMemo, useState } from 'react'
import TreeNodeData from '../../data/TreeNodeData'
import useTreeData from '../../hooks/useTreeData'
import fail from '../../util/failInlineCoalesceOperator'
import Tree from '../../data/Tree'

function TreeNode({ id }: { id: number }) {
  const { 
    nodes, 
    editingNodeIds, 
    openNodeIds, 
    selectedNodeId, 
    setNodes, 
    setOpenNodeIds, 
    setEditingNodeIds, 
    setSelectedNodeId 
  } = useTreeData()

  const node = useMemo(() => nodes.getNodeById(id) ?? fail<TreeNodeData>(), [nodes, id])
  
  const [isOpen, setIsOpen] = useState(openNodeIds.includes(id))
  const [isEditing, setIsEditing] = useState(editingNodeIds.includes(node.id))
  const [labelInput, setLabelInput] = useState(node.label)

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLabelInput(event.target.value)
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if(selectedNodeId === node.id && (isOpen || node.childrenIds.length <= 0)){
      setSelectedNodeId(0)
      return
    }
    setIsOpen(prev => !prev)
    setSelectedNodeId(node.id)
  }

  const handleClickArrow = (event: React.MouseEvent<HTMLElement>) => {
    setIsOpen(prev => !prev)
    event.stopPropagation()
  }

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter' && labelInput.length > 0) {
      finishInput()
      setIsEditing(false)
      setEditingNodeIds(prev => prev.filter(x => x !== id))
    } else if (event.key === 'Escape') {
      setLabelInput(node.label)
      setIsEditing(false)
      setEditingNodeIds(prev => prev.filter(x => x !== id))
    }
  }

  const finishInput = () => {
    setNodes(prev => prev.updateNode({...node, label: labelInput}))
  }

  useEffect(() => {
    if(isEditing && editingNodeIds.includes(node.id) === false){
      if(labelInput.length > 0){
        finishInput()
      }
      setIsEditing(false)
    }
    if(isEditing === false && editingNodeIds.includes(node.id)){
      setIsEditing(true)
    }
  }, [editingNodeIds])

  useEffect(() => {
    if(isOpen && openNodeIds.includes(node.id) === false){
      setOpenNodeIds(prev => prev.concat(node.id))
    } else if(isOpen === false && openNodeIds.includes(node.id)){
      setOpenNodeIds(prev => prev.filter(x => x !== node.id))
    }
  }, [isOpen])
  
  return (
    <li>
    <div className={'tree-node' + (selectedNodeId === node.id ? ' tree-node-selected' : '')}
      onClick={handleClick}  
    >
      {node.childrenIds.length > 0 &&
        <img src={isOpen ? "/image/arrow-down.svg" : "/image/arrow-right.svg"} 
          onClick={handleClickArrow}
          className='tree-node-arrow'
          draggable={false}
          alt='arrow'
      />
      }

      {isEditing === false ? (
        <div className='tree-node-label'>
          {node.label} {isOpen}
        </div>)
        : (
          <input type='text' 
            value={labelInput} 
            onChange={handleInput} 
            onKeyDown={handleKeyDown} 
            autoFocus
          />
        )
      }

      {isOpen && node.content !== null &&
        (<>{node.content}</>)
      }
    </div>
    {isOpen && node?.childrenIds.length > 0 &&
      <ul className='tree-node-list'>
        <div className='tree-node-children'>
          { node.childrenIds.map(childId => (<TreeNode id={childId} key={childId} />)) }
        </div>
      </ul>
    }
    </li>
  )
}

export default TreeNode