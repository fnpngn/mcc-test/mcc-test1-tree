import React, {useEffect, useState} from 'react'

function CustomCheckbox({label, defaultValue, onChange}: {
    label: string, 
    defaultValue: boolean, 
    onChange: (newValue: boolean) => void
}) {
  
  const [checked, setChecked] = useState(defaultValue)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked)
  }

  useEffect(() => {
    onChange(checked)
  }, [checked])

  return (
    <label className='styled-checkbox-container'>
      {label}  
        <input className='styled-checkbox-checkbox' type='checkbox' checked={checked} onChange={handleChange}/>
        <span className='styled-checkbox-checkmark'></span>
    </label>
  )
}

export default CustomCheckbox