import TreeNodeWrapper from "./TreeNodeWrapper"
import TreeNodeData from "./TreeNodeData"
import ITree from "./ITree"

class Tree{
    nodes: TreeNodeWrapper
    selectedNodeId: number | null
    openNodeIds: number[]
    editingNodeIds: number[]
    
    constructor(nodes: TreeNodeWrapper, selected: number | null, open: number[], editing: number[]){
        this.nodes = nodes
        this.selectedNodeId = selected
        this.openNodeIds = open
        this.editingNodeIds = editing
    }
    
    static fromNodes(nodes: TreeNodeWrapper){
        return new Tree(nodes, null, [], [])
    }
    static fromTree(tree: Tree){
        return new Tree(tree.nodes, tree.selectedNodeId, tree.openNodeIds, tree.editingNodeIds)
    }
    static fromTreeUpdateNodes(tree: Tree, nodes: TreeNodeWrapper){
        return new Tree(nodes, tree.selectedNodeId, tree.openNodeIds, tree.editingNodeIds)
    }
    static fromTreeUpdateEditing(tree: Tree, editing: number[]){
        return new Tree(tree.nodes, tree.selectedNodeId, tree.openNodeIds, editing)
    }
    static fromTreeUpdateOpen(tree: Tree, open: number[]){
        return new Tree(tree.nodes, tree.selectedNodeId, open, tree.editingNodeIds)
    }
    static fromTreeUpdateSelected(tree: Tree, selected: number | null){
        return new Tree(tree.nodes, selected, tree.openNodeIds, tree.editingNodeIds)
    }
}

export default Tree