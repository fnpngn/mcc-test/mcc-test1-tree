
class TreeNodeData {
    id: number = Math.round(Math.random() * Date.now() + Math.random())
    parentId: number = 0
    label: string = "New Tree Node"
    childrenIds: number[] = []
    content: object | null = null
}

export default TreeNodeData