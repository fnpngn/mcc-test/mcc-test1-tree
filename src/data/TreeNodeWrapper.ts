import TreeNodeData from "./TreeNodeData"
import fail from '../util/failInlineCoalesceOperator'

class TreeNodeWrapper{
    nodes: TreeNodeData[]
    constructor(nodes: TreeNodeData[]){
        this.nodes = nodes
    }
    getNodeById(id: number){
        return this.nodes.find(x => x.id === id)
    }
    getRootNode(){
        return this.getNodeById(0) ?? fail<TreeNodeData>("Root node non-existent")
    };
    getNodes(ids: number[]): TreeNodeData[]{
        return this.nodes.filter(x => ids.includes(x.id))
    }
    getNodeChildren (id: number): TreeNodeData[]{
        return this.getNodes(this.getNodeById(id)?.childrenIds ?? [])
    }   
    getAllExtendedChildrenIds (nodeId: number){
        let children: number[] = []
        let queue: number[] = [nodeId]
        let current: TreeNodeData | undefined
        while(queue.length > 0){
            current = this.getNodeById(queue.shift() ?? fail())
            if(current === undefined){
            break;
            }
            children = children.concat(current.childrenIds)
            queue = queue.concat(current.childrenIds)
        }
        return children
    } 
    updateNode (value: TreeNodeData){
        if(this.getNodeById(value.id) === undefined){
            throw new Error(`Node {id:${value.id}} does not exist`)
        }
        return new TreeNodeWrapper(
            this.nodes.filter(x => x.id !== value.id)
                .concat(value))
    }
    addNode (node: TreeNodeData): TreeNodeWrapper{
        let newNodes: TreeNodeWrapper = new TreeNodeWrapper(this.nodes)
        if(newNodes.getNodeById(node.id) !== undefined){
            throw new Error(`Node with {id: ${node.id}} already exists`)
        }
        let refNode: TreeNodeData
        node.childrenIds.forEach(refNodeId => {
            refNode = this.getNodeById(refNodeId)
                ?? fail(`Failed to add new node: referenced node {id: ${refNodeId}} does not exist`)
            newNodes = newNodes.updateNode({...refNode, parentId: node.id})
        })
        
        refNode = this.getNodeById(node.parentId)
            ?? fail(`Failed to add new node: referenced parent node {id: ${node.parentId}} does not exist`)

        newNodes = newNodes.updateNode({
            ...refNode, 
            childrenIds: refNode.childrenIds.concat(node.id)
        })
        
        
        return new TreeNodeWrapper(newNodes.nodes.concat(node))
    }
    moveNode (id: number, newParentid: number, moveChildren: boolean){
        throw new Error("Not Implemented Yet")
        return new TreeNodeWrapper([])
    }
    deleteNode (id: number, cascadeChildren: boolean): TreeNodeWrapper{
        let node: TreeNodeData = this.getNodeById(id) ?? fail(`Node {id:${id}} does not exist`)
        //remove current node
        let newNodes: TreeNodeData[] = this.nodes.filter(x => x.id !== id)
        
        if(node.childrenIds.length > 0){
            if(cascadeChildren){
                let children = this.getAllExtendedChildrenIds(node.id)
                newNodes = newNodes.filter(x => children.includes(x.id) === false)
            } else {
                //transport children
                newNodes = newNodes.filter(x => node.childrenIds.includes(x.id) === false)
                    .concat(newNodes.filter(x => node.childrenIds.includes(x.id))
                        .map(child => ({
                            ...child,
                            parentId: node.parentId
                        }))
                    )
            }
        }

        if(node.parentId === null){
            return new TreeNodeWrapper(newNodes)
        }
        //update parent
        let parent: TreeNodeData = this.getNodeById(node.parentId) ?? fail(`Node parent {id: ${node.id},parentId: ${node.parentId}} was not found`)
        //remove current node reference from parent
        let newChildren = parent.childrenIds.filter(x => x !== node.id)
        if(cascadeChildren === false){
            let nodeIndexInChildren = parent.childrenIds.indexOf(node.id)
            newChildren = parent.childrenIds.slice(0, nodeIndexInChildren).concat(node.childrenIds)
            if(nodeIndexInChildren + 1 < parent.childrenIds.length){
                newChildren = newChildren.concat(parent.childrenIds.slice(nodeIndexInChildren+1))
            }
        }
        
        return new TreeNodeWrapper(newNodes).updateNode({
            ...parent, 
            childrenIds: newChildren
        })
    }
    getPathToRoot (id: number): number[]{
        let node = this.getNodeById(id)
        let parent = node?.parentId
        let path: number[] = []
        while(parent !== undefined){
            path.push(parent)
            node = this.getNodeById(parent)
        }
        return path
    }
    getLength(): number{
        return this.nodes.length
    }
}

export default TreeNodeWrapper