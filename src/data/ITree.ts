import OperationResult from "../OperationResult"
import TreeNodeData from "./TreeNodeData"

interface ITree{
  getRootNode: () => TreeNodeData
  getNodeById: (id: number) => TreeNodeData | undefined
  getNodes: (ids: number[]) => TreeNodeData[]
  setNodesEditing: (ids: number[], state: boolean) => number[]
  setNodesOpen: (ids: number[], state: boolean) => number[]
}

export default ITree